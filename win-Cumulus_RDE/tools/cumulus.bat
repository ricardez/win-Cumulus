@echo off 
REM ###########################################################################
REM                        Updated by: Forrester Terry
REM                        Modified Date: 5/25/2016
REM                        Package Name: Cumulus 9
REM ###########################################################################


REM====================================#
REM Settings                           #
REM====================================#


SET PRODNAME=Cumulus 9
SET CommandLine=NA
SET INSTALLER=NA 
SET InstalledFolderCheck=NA
 

REM====================================#
REM DO NOT TOUCH                       #
REM====================================#


SET PRODFOLDER=NA
SET PACKAGEURL=NA
SET AM_LOG="C:\ProgramData\Stanford\CRC\AM.log"
SET LOG="C:\ProgramData\Stanford\CRC\endpoint.log"
SET CREDENTIAL=NA

IF NOT EXIST "C:\ProgramData\Stanford\CRC" (
    mkdir "C:\ProgramData\Stanford\CRC" 
) ELSE (
    echo "Log Paths exists" >> %LOG%  
)

REM PUSHD changes working Dir to location of batch file
PUSHD "%~dp0"


REM Updates date and time. Removes day of week. format: YYYY-MM-DD HH:MM:SS
For /f "tokens=2-4 delims=/ " %%a in ('date /t') do (set DATENOW=%%c-%%a-%%b)
For /f "tokens=1-3 delims=/:" %%a in ("%TIME%")  do (set Temptime=%%a:%%b:%%c)
set Temptime=%Temptime: =0%

REM Detects if system is 64 bit or not. Use this to do specific things based of Architecture 
IF EXIST "C:\Program Files (x86)" (
    SET is64bit = Yes  
) ELSE (
S   ET is64bit = No 
)

GOTO CHECKUNINSTALL
GOTO COMMENCEINSTALL
 
REM ===============================
REM <------ Begin Script -------> #

REM STEP 1
REM====================================#
REM Check to see if already installed: #
REM====================================#
REM If Application is installed already, you should either remove it, or do nothing

:CHECKUNINSTALL
REM BF will take care of updating on its own


REM # STEP 2
REM #====================================#
REM #       Begin Installer Script:      #
REM #====================================#

:COMMENCEINSTALL

echo %DATENOW% %TIMENOW% - %PRODNAME% - Starting Install. >> %LOG% 
echo %DATENOW% %TIMENOW% AM: Beginning install of %PRODNAME% >> %AM_LOG%
 
"Install_Cumulus_9_Client.exe"
copy /y "client.xml" C:\"Program Files (x86)"\Canto\"Cumulus Client"\conf
copy /y "client_ori.xml" C:\"Program Files (x86)"\Canto\"Cumulus Client"\conf

REM ---- Everything below here is post-install command material to be customized ----
  
REM --- End post-install configuration. ----


REM # STEP 3
REM #====================================#
REM # === Check to see if Installed ===  #
REM #====================================#

REM Test to see if the install was successful. Log.

IF %ERRORLEVEL%==0  ( 
    echo %DATENOW% %TIMENOW% AM: %PRODNAME% - Installed. >> %LOG% 
    echo %DATENOW% %TIMENOW% AM: Installation of %PRODNAME% successful. >> %AM_LOG%
    goto success
)
echo %DATENOW% %TIMENOW% AM: %PRODNAME% - Failed to Install. >> %LOG%
echo %DATENOW% %TIMENOW% AM: Installation of %PRODNAME% unsuccessful. >> %AM_LOG%
exit /b 1

:success

REM # <------ End Script -------> #







