﻿
$ErrorActionPreference = 'Stop';
$toolsDir   = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"
$fileLocation = Join-Path $toolsDir 'cumulus.bat'
$url        = ''
$url64      = ''

$packageArgs = @{
  packageName   = $env:ChocolateyPackageName
  unzipLocation = $toolsDir
  fileType      = 'EXE'
  url           = $url
  url64bit      = $url64
  file         = $fileLocation

  softwareName  = 'win-Cumulus_RDE*'

  checksum      = '48E82F6759373214DA9B2DFE9454A19C82064DC3A8A6CFE3179DD3C428E7C6F9'
  checksumType  = 'sha256'
  checksum64    = '48E82F6759373214DA9B2DFE9454A19C82064DC3A8A6CFE3179DD3C428E7C6F9'
  checksumType64= 'sha256'

  
  validExitCodes= @(0, 3010, 1641)
  silentArgs   = ''
}

Install-ChocolateyInstallPackage @packageArgs










    








